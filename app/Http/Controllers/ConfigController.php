<?php

namespace App\Http\Controllers;

class ConfigController extends Controller
{
    public function getBaseURL()
    {
        $config = [
            'baseURL' => env('API_BASE_URL')
        ];
        
        return response()->json($config);
    }
}
