import "./bootstrap"
import Vue from "vue"
import { BootstrapVue, BIcon, BIconArrowUp, BIconArrowDown } from 'bootstrap-vue'
import Register from '@/js/views/Register'
import "bootstrap-vue/dist/bootstrap-vue.css"
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faExclamationTriangle  } from '@fortawesome/free-solid-svg-icons'
import axios from 'axios';

Vue.use(BootstrapVue);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.prototype.$axios = axios;

library.add(faExclamationTriangle )

window.onload = function() {
    const app = new Vue({
        el: '#app',
        render: h => h(Register)
    });
}

export default app;